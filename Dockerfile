FROM php:7.0-apache
COPY . /var/www/html
EXPOSE 8721
CMD ["php", "-S", "0.0.0.0:8721"]